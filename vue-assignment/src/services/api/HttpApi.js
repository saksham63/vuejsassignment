import axios from 'axios';

export default {
    login(email, password) {
        return axios.post('/users/login', {
            user: {
                email: email,
                password: password
            }
        }).then((response) => {
            var user = response.data.user;
            localStorage.userAuthToken = user.token;
            localStorage.username = user.username;
        });
    },
    signup(userToBeCreated) {
        return axios.post('/users', {
            user: userToBeCreated
        });
    },
    getArticles(offset) {
        return axios.get('/articles?limit=10&offset=' + offset, {
            headers: this.getHeadersForRequest()
        });
    },
    getUserFeed(offset) {
        return axios.get('/articles/feed?limit=10&offset=' + offset, {
            headers: this.getHeadersForRequest()
        });
    },
    filterArticlesByTag(offset, tag) {
        return axios.get('/articles?limit=10&offset=' + offset + '&tag=' + tag, {
            headers: this.getHeadersForRequest()
        });
    },
    getUserArticles(offset) {
        return axios.get('/articles?author='+this.getUserName()+'&limit=10&offset=' + offset, {
            headers: this.getHeadersForRequest()
        });
    },
    getUserFavoritedArticles(offset) {
        return axios.get('/articles?favorited='+this.getUserName()+'&limit=10&offset=' + offset, {
            headers: this.getHeadersForRequest()
        });
    },
    deleteFavoriteForArticle(slug) {
        return axios.delete('/articles/' + slug + '/favorite', {
            headers: this.getHeadersForRequest()
        });
    },
    createFavoriteForArticle(slug) {
        return axios.post('/articles/' + slug + '/favorite', {}, {
            headers: this.getHeadersForRequest()
        });
    },
    getArticle(slug) {
        return axios.get('/articles/' + slug, {
            headers: this.getHeadersForRequest()
        });
    },
    getArticleComments(slug) {
        return axios.get('/articles/' + slug + '/comments', {
            headers: this.getHeadersForRequest()
        });
    },
    postComment(commentBody, slug) {
        return axios.post('/articles/' + slug + '/comments', {
            comment: {
                body: commentBody
            }
        }, { headers: this.getHeadersForRequest() });
    },
    deleteComment(articleSlug, commentId) {
        return axios.delete('/articles/' + articleSlug + '/comments/' + commentId, {
            headers: this.getHeadersForRequest()
        })
    },
    followUser(username) {
        return axios.post('/profiles/' + username + '/follow', {}, {
            headers: this.getHeadersForRequest()
        });
    },
    unfollowUser(username) {
        return axios.delete('/profiles/' + username + '/follow', {
            headers: this.getHeadersForRequest()
        });
    },
    publishArticle(articleToBePublished) {
        return axios.post('/articles', { article: articleToBePublished }, {
            headers: this.getHeadersForRequest()
        });
    },
    updateArticle(articleToBeUpdated) {
        return axios.put('/articles/' + articleToBeUpdated.slug, {
            article: articleToBeUpdated
        }, {
                headers: this.getHeadersForRequest()
            });
    },
    deleteArticle(articleSlug) {
        return axios.delete('/articles/' + articleSlug, {
            headers: this.getHeadersForRequest()
        });
    },
    getAllTags() {
        return axios.get('/tags');
    },
    getUser() {
        return axios.get('/profiles/' + this.getUserName());
    },
    isUserLoggedIn() {
        return localStorage.userAuthToken !== undefined
    },
    getUserName() {
        return localStorage.username;
    },
    getHeadersForRequest() {
        if (this.isUserLoggedIn()) {
            return { authorization: 'Token ' + localStorage.userAuthToken };
        }
        else {
            return {};
        }
    }
}