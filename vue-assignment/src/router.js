import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import ArticleDetail from './views/ArticleDetail.vue';
import NewArticle from './views/NewArticle.vue';
import EditArticle from './views/EditArticle.vue';
import UserInfo from './views/UserInfo.vue';
import Signup from './views/Signup.vue';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        title: 'Home --- Conduit'
      }
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
      meta: {
        title: 'Login'
      }
    },
    {
      path: '/article/:slug',
      name: 'ArticleDetail',
      component: ArticleDetail
    },
    {
      path: '/new-article',
      name: 'NewArticle',
      component: NewArticle
    },
    {
      path: '/edit-article/:slug',
      name: 'EditArticle',
      component: EditArticle
    },
    {
      path: '/user',
      name: 'UserInfo',
      component: UserInfo
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    }
  ]
});
